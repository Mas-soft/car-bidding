package com.car.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.car.model.Bid;
import com.car.model.Bidding;
import com.car.model.Car;
import com.car.model.FavouriteBidding;
import com.car.model.User;
import com.car.repository.BidRepository;
import com.car.repository.BiddingRepository;
import com.car.repository.CarRepository;
import com.car.repository.CurrencyRepository;
import com.car.repository.FavouriteBiddingsRepository;
import com.car.repository.UserRepository;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;



@RestController
public class BiddingController {
	
	@Autowired
	UserRepository userService;
	
	@Autowired
	BiddingRepository biddingService;
	
	@Autowired
	BidRepository bidService;
	
	@Autowired
	CarRepository carService;
	
	@Autowired
	FavouriteBiddingsRepository favouriteBiddingService;
	
	@Autowired
    ServletContext context; 
	
	@Autowired
	CurrencyRepository currencyService;
	
	@PostMapping(path="/api/getBiddings")
	public ResponseEntity<Map<String,Object>> GetBiddings(@RequestBody Map<String,String> userData, @RequestParam(name="state", required=false) String state) {
		
		Map<String,Object> retval=new HashMap<>();
		retval.put("biddings", biddingService.findAllWithState(state));
		
		return ResponseEntity.ok().body(retval);
		
	}
	
	@GetMapping(path="/api/getBiddings")
	public ResponseEntity<Map<String,Object>> Biddings(@RequestParam(name="state", required=false) String state){
		
		Map<String,Object> retval = new HashMap<>();
		List<Bidding> biddings = biddingService.findAllWithState(state);
		if(!biddings.isEmpty()) {
			retval.put("biddings", biddings);
			return ResponseEntity.ok().body(retval);
		}
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping(path="/api/getMyBiddings")
	public ResponseEntity<Map<String, Object>> getMyBiddings(@RequestBody Map<String,String> userData) throws ParseException{
		Map<String,Object> retval=new HashMap<>();
		Integer id=Integer.parseInt(userData.get("id"));
		Optional<User> userOpt= userService.findById(id);
		String accessToken=userData.get("accessToken");
		if (userOpt.isPresent() && userOpt.get().getAccessToken().equals(accessToken)) {
			
			String state = userData.get("state");
			List<Bidding> myBiddings = biddingService.findMyBiddings(state, id);
			
			retval.put("myBiddings", myBiddings);
			return ResponseEntity.ok().body(retval);
		}
		return ResponseEntity.notFound().build();
		
	}
	
	@GetMapping(path="/api/Bidding")
	public ResponseEntity<Map<String,Object>> singleBidding(@RequestParam("id") int biddingId){
		
		Map<String,Object> retval = new HashMap<>();
		Optional<Bidding> bidOpt = biddingService.findById(biddingId);
		if(bidOpt.isPresent()) {
			retval.put("bidding", bidOpt);
			return ResponseEntity.ok().body(retval);
		}
		
		return ResponseEntity.notFound().build();
		
	}
	
	@PostMapping(path="/api/newBidding")
	public ResponseEntity<Map<String,Object>> NewBidding( @RequestParam(name="biddingData") String biddingData, @RequestParam(name="img") MultipartFile img) throws ParseException {
		
		
		JSONParser parser = new JSONParser();
		JSONObject json = null;
		try {
			json = (JSONObject) parser.parse(biddingData);
		} catch(net.minidev.json.parser.ParseException e) {
			e.printStackTrace();
		}
		
		Map<String,Object> retval=new HashMap<>();
		Optional<User> userOpt= userService.findById(Integer.parseInt(json.get("id").toString()));
		if (userOpt.isPresent() && userOpt.get().getAccessToken().equals(json.get("accessToken"))) {
			
			Bidding bid=new Bidding();
			int carId=Integer.parseInt(json.get("carId").toString());
			Car car =carService.findById(carId).get();
			bid.setCar(car);
			bid.setBuyNowPrice(json.get("buyNowPrice").toString());
			bid.setStartingPrice(json.get("startingPrice").toString());
			bid.setCreatedAt(new Date());
			bid.setTitle(json.get("title").toString());
			bid.setDescription(json.get("description").toString());
			bid.setActive(false);
			bid.setMinRate(json.get("minRate").toString());
			int currencyId=Integer.parseInt(json.get("currencyId").toString());
			bid.setCurrency(currencyService.findById(currencyId).get());
			Date startTime=new SimpleDateFormat("yyyy-MM-dd").parse((String)json.get("startTime"));
			bid.setStartTime(startTime);
			bid.setDays(json.get("days").toString());
//			Date endTime = new SimpleDateFormat("yyyy-MM-dd").parse((String)json.get("endTime"));
//			bid.setEndTime(endTime);
			
			
			if (!img.isEmpty()) {
	 			try {
	 				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	 				byte[] bytes = img.getBytes();
	 				
	 				// Creating the directory to store file
	 				String contextPath = context.getRealPath(File.separator);
	 				File dir = new File(contextPath +File.separator + "uploads");
	 				if (!dir.exists())
	 					dir.mkdirs();

	 				// Create the file on server
	 				String imageName=img.getOriginalFilename();
	 				String filePath = "CarImage_"+timestamp.getTime();
	 				String serverFile = dir.getAbsolutePath()+ File.separator +filePath;
	 				BufferedOutputStream stream = new BufferedOutputStream(Files.newOutputStream(Paths.get(serverFile)));
	 				stream.write(bytes);
	 				stream.close();
	 				bid.setBiddingPhoto(File.separator + "uploads"+File.separator+filePath);
	 				
	 			}catch(Exception e) {
	 				
	 			}
			}
			
			bid=biddingService.save(bid);
			retval.put("bid", bid);
			return ResponseEntity.ok().body(retval);
		}
		return ResponseEntity.notFound().build();
		
	}
	
	@PostMapping(path="/api/newBid")
	public ResponseEntity<Map<String,Object>> NewBid( @RequestBody Map<String,String> userData) throws ParseException {
		Map<String,Object> retval=new HashMap<>();
		int id=Integer.parseInt(userData.get("id"));
		Optional<User> userOpt= userService.findById(id);
		String accessToken=userData.get("accessToken");
		if (userOpt.isPresent() && userOpt.get().getAccessToken().equals(accessToken)) {
			Bid bid =new Bid();
			Bidding bidding = biddingService.findById(Integer.parseInt(userData.get("biddingId"))).get();
			bidding.setLastPrice(userData.get("price"));
			bid.setBidding(bidding);
			bid.setCreatedAt(new Date());
			bid.setUser(userOpt.get());
			bid.setPrice(userData.get("price"));
			bid=bidService.save(bid);
			retval.put("bid", bid);
			return ResponseEntity.ok().body(retval);
		}
		return ResponseEntity.notFound().build();
		
	}
	
	@GetMapping(path="/api/s")
	public ResponseEntity<Map<String, Object>> getSearchResult(@RequestParam(name="brand", required=false) String brand, @RequestParam(name="model", required=false) String model,
			@RequestParam(name="year", required=false) String year, @RequestParam(name="price", required=false) String price, @RequestParam(name="state", required=false) String state){
		System.out.println("brand: " + brand + ", model: " + model + ", year: " + year + ", price: " + price + ", state: " + state);
		Map<String, Object> retval = new HashMap<>();
		List<Bidding> biddings = null;
		
		biddings = biddingService.search(brand, model, year, price, state);
		
		retval.put("biddings", biddings);
		return ResponseEntity.ok().body(retval);
	}

}


















