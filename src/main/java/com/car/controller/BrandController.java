package com.car.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.car.repository.BrandRepository;

@RestController
public class BrandController {
	
	@Autowired
	private BrandRepository brandRepo;
	
	@GetMapping(path="/api/getBrands")
	public ResponseEntity<Map<String,Object>> getBrands(){
		
		Map<String,Object> retval = new HashMap<>();
		retval.put("brands", brandRepo.findAll());
		
		return ResponseEntity.ok().body(retval);
		
	}

}
