package com.car.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.car.model.Car;
import com.car.model.CarImage;
import com.car.model.FavouriteBidding;
import com.car.model.ImageReviews;
import com.car.model.Model;
import com.car.model.User;
import com.car.repository.CarImageRepository;
import com.car.repository.CarRepository;
import com.car.repository.ImageReviewsRepository;
import com.car.repository.ModelRepository;
import com.car.repository.UserRepository;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;



@RestController
public class CarController {
	
	@Autowired
	UserRepository userService;
	
	@Autowired
	CarRepository carService;
	
	@Autowired
	CarImageRepository imgService;
	
	@Autowired
	ImageReviewsRepository reviewService;
	
	@Autowired
    ServletContext context; 
	
	@Autowired
	ModelRepository modelRepo;
	
	
	@PostMapping(path="/api/getCars")
	public ResponseEntity<Map<String,Object>> GetCars(@RequestBody Map<String,String> userData) {
		
		Map<String,Object> retval=new HashMap<>();
		
			
			retval.put("cars", carService.findAll());
			
			return ResponseEntity.ok().body(retval);
		
		
		
	}
	
	@PostMapping(path="/api/newCar")
	public ResponseEntity<Map<String,Object>> NewCar( @RequestParam(name="carData") String carData , @RequestParam(name="certificate", required = false) MultipartFile certificate) {
		
		JSONParser parser = new JSONParser();
		JSONObject json = null;
		try {
			json = (JSONObject) parser.parse(carData);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Map<String,Object> retval=new HashMap<>();
		Optional<User> userOpt= userService.findById(Integer.parseInt(json.get("id").toString()));
		if (userOpt.isPresent() && userOpt.get().getAccessToken().equals(json.get("accessToken"))) {
			
			String km=json.get("km").toString();
			String year=json.get("year").toString();
			int modelId=Integer.parseInt(json.get("modelId").toString());
			Optional<Model> modelOpt = modelRepo.findById(modelId);
			Car car =new Car();
			car.setKm(km);
			car.setCreatedAt(new Date());
			car.setUser(userOpt.get());
			car.setYear(year);
			car.setModel(modelOpt.get());
			if (certificate != null && !certificate.isEmpty()) {
	 			try {
	 				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	 				byte[] bytes = certificate.getBytes();
	 				
	 				// Creating the directory to store file
	 				String contextPath = context.getRealPath(File.separator);
	 				File dir = new File(contextPath +File.separator + "uploads");
	 				if (!dir.exists())
	 					dir.mkdirs();

	 				// Create the file on server
	 				String imageName=certificate.getOriginalFilename();
	 				String filePath = "CarImage_"+timestamp.getTime();
	 				String serverFile = dir.getAbsolutePath()+ File.separator +filePath;
	 				BufferedOutputStream stream = new BufferedOutputStream(Files.newOutputStream(Paths.get(serverFile)));
	 				stream.write(bytes);
	 				stream.close();
	 				car.setCertificate(File.separator + "uploads"+File.separator+filePath);
	 				
	 			}catch(Exception e) {
	 				
	 			}
			}else {
				car.setCertificate(null);
			}
			
			car=carService.save(car);
			retval.put("car", car);
			return ResponseEntity.ok().body(retval);
		}
		return ResponseEntity.notFound().build();
		
	}
	
	@PostMapping(path="/api/newCarImage")
	public ResponseEntity<Map<String,Object>> NewCarImage( @RequestParam(name="imgData") String userData ,@RequestParam(name="img") MultipartFile img) {
		
		JSONParser parser = new JSONParser();
		JSONObject json = null;
		try {
			json = (JSONObject) parser.parse(userData);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Map<String,Object> retval=new HashMap<>();
		Optional<User> userOpt= userService.findById(Integer.parseInt(json.get("id").toString()));
		if (userOpt.isPresent() && userOpt.get().getAccessToken().equals(json.get("accessToken"))) {
			Car car =carService.findById(Integer.parseInt(json.get("carId").toString())).get();
			CarImage cImg=new CarImage();
			cImg.setCar(car);
			if (!img.isEmpty()) {
	 			try {
	 				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	 				byte[] bytes = img.getBytes();
	 				
	 				// Creating the directory to store file
	 				String contextPath = context.getRealPath(File.separator);
	 				File dir = new File(contextPath +File.separator + "uploads");
	 				if (!dir.exists())
	 					dir.mkdirs();

	 				// Create the file on server
	 				String imageName=img.getOriginalFilename();
	 				String filePath = "CarImage_"+timestamp.getTime();
	 				String serverFile = dir.getAbsolutePath()+ File.separator +filePath;
	 				BufferedOutputStream stream = new BufferedOutputStream(Files.newOutputStream(Paths.get(serverFile)));
	 				stream.write(bytes);
	 				stream.close();
	 				cImg.setImage(File.separator + "uploads"+File.separator+filePath);
	 				cImg.setImageName(imageName);
	 				
	 				imgService.save(cImg);
	 				
	 				retval.put("carImg", cImg);
//	 				retval.put("carImg", car.getId());
//	 				retval.put("imgId", imgService.save(cImg).getId());
	 			}catch(Exception e) {
	 				
	 			}
			}

			return ResponseEntity.ok().body(retval);
		}
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping(path="/api/getReviews")
	public ResponseEntity<Map<String, Object>> getReviews(@RequestBody Map<String, String> userData){
		Map<String,Object> retval=new HashMap<>();
		int id=Integer.parseInt(userData.get("id"));
		Optional<User> userOpt= userService.findById(id);
		String accessToken=userData.get("accessToken");
		if (userOpt.isPresent() && userOpt.get().getAccessToken().equals(accessToken)) {
			
			int biddingId = Integer.parseInt(userData.get("biddingId"));
			List<ImageReviews> reviews = reviewService.findByBiddingAndUser(id, biddingId);
			
			retval.put("reviews", reviews);
			return ResponseEntity.ok().body(retval);
			
		}
		
		return ResponseEntity.notFound().build();

	}
	
	@PostMapping(path="/api/newReview")
	public ResponseEntity<Map<String,Object>> NewReview( @RequestBody Map<String,String> userData) {
		
		Map<String,Object> retval=new HashMap<>();
		int id=Integer.parseInt(userData.get("id"));
		Optional<User> userOpt= userService.findById(id);
		String accessToken=userData.get("accessToken");
		if (userOpt.isPresent() && userOpt.get().getAccessToken().equals(accessToken)) {
			
				int imgId=Integer.parseInt(userData.get("imgId"));
				CarImage img=imgService.findById(imgId).get();
				Optional<ImageReviews> imgReview = reviewService.findByCarImageId(img.getId());
				ImageReviews review;
				
				if(!imgReview.isPresent()) {
				
					int valid=Integer.parseInt(userData.get("valid"));
					review = new ImageReviews();
					review.setCarImage(img);
					review.setUser(userOpt.get());
					if(valid==1) {
						review.setValid(true);
					}else {
						review.setValid(false);
					}
					
					reviewService.save(review);
				}else {
					review = imgReview.get();
					review.setValid(!review.getValid());
					reviewService.save(review);
				}
			retval.put("review", review);
			retval.put("carId", img.getId());
			
			return ResponseEntity.ok().body(retval);
		}
		return ResponseEntity.notFound().build();
		
	}
	
	@PostMapping(path="/api/removeReview")
	public ResponseEntity<Map<String, String>> removeReview(@RequestBody Map<String, String> userData) throws ParseException{
		Map<String, String> retval = new HashMap<>();
		Integer id=Integer.parseInt(userData.get("id"));
		Optional<User> userOpt= userService.findById(id);
		String accessToken=userData.get("accessToken");
		if (userOpt.isPresent() && userOpt.get().getAccessToken().equals(accessToken)) {
			
			CarImage carImage = imgService.findById(Integer.parseInt(userData.get("imgId"))).get();
			Optional<ImageReviews> imgReviewOpt = reviewService.findByCarImageIdAndUserId(carImage.getId(), id);
			Boolean reviewState = null;
			if(imgReviewOpt.isPresent()) {
				ImageReviews imgReview = imgReviewOpt.get();
				reviewState = imgReview.getValid();
				reviewService.delete(imgReview);
			}
			
			retval.put("operation", "success");
			retval.put("state", reviewState.toString());
			
			return ResponseEntity.ok(retval);
		}
		
		return ResponseEntity.notFound().build();
	}
	

}
