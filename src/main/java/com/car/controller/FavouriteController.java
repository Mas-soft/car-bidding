package com.car.controller;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.car.model.Bidding;
import com.car.model.FavouriteBidding;
import com.car.model.User;
import com.car.repository.BiddingRepository;
import com.car.repository.FavouriteBiddingsRepository;
import com.car.repository.UserRepository;

@RestController
public class FavouriteController {

	
	@Autowired
	UserRepository userService;
	
	@Autowired
	FavouriteBiddingsRepository favouriteBiddingService;
	
	@Autowired
	BiddingRepository biddingService;
	
	@PostMapping(path="/api/getFavourites")
	public ResponseEntity<Map<String, Object>> getFavouriteBiddings(@RequestBody Map<String,String> userData) throws ParseException{
		Map<String,Object> retval=new HashMap<>();
		Integer id=Integer.parseInt(userData.get("id"));
		Optional<User> userOpt= userService.findById(id);
		String accessToken=userData.get("accessToken");
		if (userOpt.isPresent() && userOpt.get().getAccessToken().equals(accessToken)) {
			
			String state = userData.get("state");
			List<FavouriteBidding> favouriteBiddings = favouriteBiddingService.findAllFavouriteBiddingsByUserId(id, state);
			
			retval.put("favouriteBiddings", favouriteBiddings);
			return ResponseEntity.ok().body(retval);
		}
		return ResponseEntity.notFound().build();
		
	}
	
	@PostMapping(path="/api/addToFavorites")
	public ResponseEntity<Map<String, Object>> addToFavourites(@RequestBody Map<String, String> userData) throws ParseException{
		Map<String, Object> retval = new HashMap<>();
		Integer id=Integer.parseInt(userData.get("id"));
		Optional<User> userOpt= userService.findById(id);
		String accessToken=userData.get("accessToken");
		if (userOpt.isPresent() && userOpt.get().getAccessToken().equals(accessToken)) {
			
			FavouriteBidding favBidding = new FavouriteBidding();
			favBidding.setUser(userOpt.get());
			Optional<Bidding> biddingOpt = biddingService.findById(Integer.parseInt(userData.get("biddingId")));
			favBidding.setBidding(biddingOpt.get());
			
			favouriteBiddingService.save(favBidding);
			
			retval.put("favouriteBidding", favBidding);
			
			return ResponseEntity.ok(retval);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping(path="/api/removeFromFavorites")
	public ResponseEntity<Map<String, String>> removeFromFavourites(@RequestBody Map<String, String> userData) throws ParseException{
		Map<String, String> retval = new HashMap<>();
		Integer id=Integer.parseInt(userData.get("id"));
		Optional<User> userOpt= userService.findById(id);
		String accessToken=userData.get("accessToken");
		if (userOpt.isPresent() && userOpt.get().getAccessToken().equals(accessToken)) {
			
			FavouriteBidding favBidding = favouriteBiddingService.findById(Integer.parseInt(userData.get("favoriteId"))).get();
			favouriteBiddingService.delete(favBidding);
			
			retval.put("operation", "success");
			
			return ResponseEntity.ok(retval);
		}
		
		return ResponseEntity.notFound().build();
	}
}
