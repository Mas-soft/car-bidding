package com.car.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.car.model.User;
import com.car.repository.UserRepository;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;



@RestController
public class UserController {
	
	@Autowired
	UserRepository userService;
	
	@Autowired
    ServletContext context; 
	
	
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}
	
	@PostMapping(path="/api/login")
	public ResponseEntity<Map<String,String>> login(@Valid @RequestBody User user) {
		
		Map<String,String> retval=new HashMap<>();
		Optional<User> userOpt= userService.findByEmail(user.getEmail());
		if (userOpt.isPresent() && userOpt.get().getPassword().equals(user.getPassword())) {
			user =userOpt.get();
			
			String accessToken=randomAlphaNumeric(20);
			user.setAccessToken(accessToken);
			userService.save(user);
			retval.put("id", user.getId().toString());
			retval.put("accessToken", accessToken);
			return ResponseEntity.ok().body(retval);
		}
		
		return ResponseEntity.notFound().build();
		
		
	}
	
	@PostMapping(path="/api/getUser")
	public ResponseEntity<Map<String,Object>> getUser(@RequestBody Map<String,String> userData) {
		
		Map<String,Object> retval=new HashMap<>();
		
		int id = Integer.parseInt(userData.get("id"));
		Optional<User> userOpt= userService.findById(id);
		
		String accessToken=userData.get("accessToken");
		
		if (userOpt.isPresent() && userOpt.get().getAccessToken().equals(accessToken)) {
			User userTemp =userOpt.get();
			retval.put("user", userTemp);
			return ResponseEntity.ok().body(retval);
		}
		
		return ResponseEntity.notFound().build();
		
		
	}
	
	
	@PostMapping(path="/api/register")
	public ResponseEntity<Map<String,String>> Register( @RequestBody Map<String,String> userData) {
		
		Map<String,String> retval=new HashMap<>();
		String email=userData.get("email");
		Optional<User> userOpt= userService.findByEmail(email);
		if (userOpt.isPresent() ) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		}
		String accessToken=randomAlphaNumeric(20);
		User user=new User();
		user.setEmail(email);
		user.setCreatedAt(new Date());
		user.setDevice(userData.get("device"));
		user.setName(userData.get("name"));
		user.setPassword(userData.get("password"));
		user.setPhone(userData.get("phone"));	
		user.setAccessToken(accessToken);
		user=userService.save(user);
		retval.put("id", user.getId().toString());
		retval.put("accessToken", accessToken);
		return ResponseEntity.ok().body(retval);
		
	}
	
	@PostMapping(path="/api/saveProfileInfo")
	public ResponseEntity<Map<String,String>> saveProfileInfo( @RequestParam(name="userData") String userData, @RequestParam(name="img", required = false) MultipartFile img) {
		
		JSONParser parser = new JSONParser();
		JSONObject json = null;
		try {
			json = (JSONObject) parser.parse(userData);
		} catch(net.minidev.json.parser.ParseException e) {
			e.printStackTrace();
		}
		
		Map<String,String> retval=new HashMap<>();
		
		int id = Integer.parseInt(json.get("id").toString());
		String accessToken = json.get("accessToken").toString();
		String oldPassword = json.get("oldPassword").toString();
		
		Optional<User> userOpt = userService.findById(id);
		User user = userOpt.get();
		
		if (userOpt.isPresent() && user.getAccessToken().equals(accessToken) && user.getPassword().equals(oldPassword)) {
			
			String newPassword = json.get("newPassword").toString();
			if(!newPassword.equals("")) {
				user.setPassword(newPassword);	
			}
			
			user.setEmail(json.get("email").toString());
			user.setName(json.get("name").toString());
			user.setPhone(json.get("mobile").toString());
			
			
			if (img != null && !img.isEmpty()) {
	 			try {
	 				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	 				byte[] bytes = img.getBytes();
	 				
	 				// Creating the directory to store file
	 				String contextPath = context.getRealPath(File.separator);
	 				File dir = new File(contextPath +File.separator + "uploads");
	 				if (!dir.exists())
	 					dir.mkdirs();

	 				// Create the file on server
	 				String imageName=img.getOriginalFilename();
	 				String filePath = "CarImage_"+timestamp.getTime();
	 				String serverFile = dir.getAbsolutePath()+ File.separator +filePath;
	 				BufferedOutputStream stream = new BufferedOutputStream(Files.newOutputStream(Paths.get(serverFile)));
	 				stream.write(bytes);
	 				stream.close();
	 				user.setProfilePhoto(File.separator + "uploads"+File.separator+filePath);
	 				
	 			}catch(Exception e) {
	 				
	 			}
			}else {
				
				String profilePhoto = user.getProfilePhoto();
				
				if(profilePhoto == null) {
					user.setProfilePhoto(null);
				}else {
					user.setProfilePhoto(profilePhoto);
				}
			}
			
			
			userService.save(user);
			
			retval.put("id", user.getId().toString());
			retval.put("accessToken", user.getAccessToken());
			
			return ResponseEntity.ok().body(retval);
		}
		return ResponseEntity.notFound().build();

	}
	

}
