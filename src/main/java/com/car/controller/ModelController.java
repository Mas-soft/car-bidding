package com.car.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.car.model.Model;
import com.car.repository.ModelRepository;

@RestController
public class ModelController {

	@Autowired
	private ModelRepository modelRepo;
	
	@GetMapping(path="/api/getModels")
	public ResponseEntity<Map<String,Object>> getModels(@RequestParam("brand") String brand){
		
		Map<String,Object> retval=new HashMap<>();
		List<Model> models = modelRepo.findAllByBrand(brand);
		retval.put("models", models);
		return ResponseEntity.ok().body(retval);
		
	}
	
}
