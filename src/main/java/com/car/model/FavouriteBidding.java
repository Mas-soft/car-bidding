package com.car.model;


// Generated Dec 3, 2019 11:31:20 AM by Hibernate Tools 5.1.10.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * FavouriteBidding generated by hbm2java
 */
@Entity
@Table(name = "favourite_bidding", catalog = "carbidding")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FavouriteBidding implements java.io.Serializable {

	private Integer favouriteId;
	private Bidding bidding;
	private User user;

	public FavouriteBidding() {
	}

	public FavouriteBidding(Bidding bidding, User user) {
		this.bidding = bidding;
		this.user = user;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "favouriteId", unique = true, nullable = false)
	public Integer getFavouriteId() {
		return this.favouriteId;
	}

	public void setFavouriteId(Integer favouriteId) {
		this.favouriteId = favouriteId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bidding_id", foreignKey= @ForeignKey(name = "fk_favourite_bidding_bidding"))
	public Bidding getBidding() {
		return this.bidding;
	}

	public void setBidding(Bidding bidding) {
		this.bidding = bidding;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", foreignKey= @ForeignKey(name = "fk_favourite_bidding_user"))
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
