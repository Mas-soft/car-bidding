package com.car.model;
// Generated Sep 9, 2019 1:59:35 PM by Hibernate Tools 5.3.0.Beta2

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Bid generated by hbm2java
 */
@Entity
@Table(name = "bid")
public class Bid implements java.io.Serializable {

	private Integer id;
	private Bidding bidding;
	private User user;
	private Date createdAt;
	private String price;
	private String userName;

	public Bid() {
	}

	public Bid(Bidding bidding, User user) {
		this.bidding = bidding;
		this.user = user;
	}

	public Bid(Bidding bidding, User user, Date createdAt, String price) {
		this.bidding = bidding;
		this.user = user;
		this.createdAt = createdAt;
		this.price = price;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "bidding_id", foreignKey= @ForeignKey(name = "fk_bid_bidding1"), nullable = true)
	@JsonIgnore
	public Bidding getBidding() {
		return this.bidding;
	}

	public void setBidding(Bidding bidding) {
		this.bidding = bidding;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", foreignKey= @ForeignKey(name = "fk_bid_user1"), nullable = true)
	@JsonIgnore
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "created_at", length = 10)
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "price", length = 45)
	public String getPrice() {
		return this.price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	@Transient
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@PostLoad
	void onLoad() {

		this.userName=user.getName();
	}

}
