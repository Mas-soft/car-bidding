package com.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.car.model.Currency;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency,Integer> {

}
