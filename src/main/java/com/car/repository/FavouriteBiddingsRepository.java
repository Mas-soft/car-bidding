package com.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.car.model.FavouriteBidding;

public interface FavouriteBiddingsRepository extends JpaRepository<FavouriteBidding, Integer>, FavouriteBiddingRepositoryCustom {

}
