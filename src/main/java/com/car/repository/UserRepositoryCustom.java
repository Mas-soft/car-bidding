package com.car.repository;

import java.util.Optional;

import com.car.model.User;


public interface UserRepositoryCustom {

	Optional<User> findByEmail(String email);
	
}
