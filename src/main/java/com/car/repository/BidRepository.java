package com.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.car.model.Bid;

@Repository
public interface BidRepository extends JpaRepository<Bid,Integer>, BidRepositoryCustom {

}
