package com.car.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.car.model.User;

@Repository
@Transactional(readOnly = true)
public class UserRepositoryImpl implements UserRepositoryCustom {

	@PersistenceContext
    EntityManager entityManager;
	
	
	@Override
	public Optional<User> findByEmail(String email) {
		String queryString="SELECT u.* from user as u WHERE u.email LIKE ?";
		Query query = entityManager.createNativeQuery(queryString, User.class);
        query.setParameter(1, email);
        List<User> users=query.getResultList();
        return users.isEmpty() ? Optional.empty() : Optional.of( users.get(0) );
	}

}
