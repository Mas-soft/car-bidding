package com.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.car.model.CarImage;

@Repository
public interface CarImageRepository extends JpaRepository<CarImage,Integer> {

}
