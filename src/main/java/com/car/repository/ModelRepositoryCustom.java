package com.car.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.car.model.Model;

public interface ModelRepositoryCustom {

	@Query("SELECT m from Model as m WHERE m.brand.brand = ?1")
	List<Model> findAllByBrand(String brand);
}
