package com.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.car.model.Model;

@Repository
public interface ModelRepository extends JpaRepository<Model, Integer>, ModelRepositoryCustom{

}
