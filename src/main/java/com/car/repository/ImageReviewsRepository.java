package com.car.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.car.model.ImageReviews;

@Repository
public interface ImageReviewsRepository extends JpaRepository<ImageReviews,Integer>, ImageReviewsRepositoryCustom {

}
