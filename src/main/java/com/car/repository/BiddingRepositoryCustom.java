package com.car.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.car.model.Bidding;

public interface BiddingRepositoryCustom {

	@Query("SELECT b from Bidding as b WHERE b.car.model.brand.brand = :brand and b.car.model.model = :model "
			+ "and (:year = '' or b.car.year = :year) and (:price = '' or (b.startingPrice > 0 and b.startingPrice >= :price)) and (:state = '' or b.active = :state)")
	public List<Bidding> search(@Param("brand") String brand, @Param("model") String model, @Param("year") String year, @Param("price") String price, @Param("state") String state);
	
	@Query("SELECT b from Bidding as b where (?1 = '' or b.active = ?1)")
	public List<Bidding> findAllWithState(String state);
	
	@Query("SELECT b from Bidding as b WHERE b.car.user.id = ?2 and (?1 = '' or b.active = ?1)")
	public List<Bidding> findMyBiddings(String state, Integer id);
}