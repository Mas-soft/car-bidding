package com.car.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;

import com.car.model.ImageReviews;

public interface ImageReviewsRepositoryCustom {
	
	@Query(value="Select r from ImageReviews as r where r.carImage.id = ?1")
	public Optional<ImageReviews> findByCarImageId(Integer imgId);
	
	@Query(value="Select r from ImageReviews as r where r.carImage.id = ?1 and r.user.id = ?2")
	public Optional<ImageReviews> findByCarImageIdAndUserId(Integer imgId, Integer userId);
	
	@Query(value="SELECT ir.* FROM carbidding.image_reviews ir"
			+ " join carbidding.car_image ci on ci.id=ir.car_image_id "
			+ "join carbidding.bidding as b on b.car_id=ci.car_id "
			+ "where b.id= ?2 and ir.user_id= ?1", nativeQuery = true)
	public List<ImageReviews> findByBiddingAndUser(Integer userId, Integer biddingId);

}
