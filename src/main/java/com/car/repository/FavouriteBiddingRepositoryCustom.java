package com.car.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.car.model.FavouriteBidding;

public interface FavouriteBiddingRepositoryCustom {
	
	@Query("SELECT f from FavouriteBidding as f WHERE f.user.id = ?1 and (?2 = '' or f.bidding.active = ?2)")
	public List<FavouriteBidding> findAllFavouriteBiddingsByUserId(Integer userId, String state);
}
