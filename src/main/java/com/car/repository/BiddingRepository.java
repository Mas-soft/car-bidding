package com.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.car.model.Bidding;

@Repository
public interface BiddingRepository extends JpaRepository<Bidding,Integer>, BiddingRepositoryCustom {

}
