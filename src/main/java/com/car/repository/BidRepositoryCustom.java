package com.car.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;

public interface BidRepositoryCustom {

	@Query(value="select max(price) from Bid as b where b.bidding.id = ?1")
	public Optional<String> findMaxBid(int bidId); 
}
