package com.car;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.car.BiddingApp;


@SpringBootApplication
//@EnableConfigurationProperties(StorageProperties.class)
public class BiddingApp extends SpringBootServletInitializer {

	
	
	public static void main(String[] args) {
		SpringApplication.run(BiddingApp.class, args);
	}

	/*@Bean
    CommandLineRunner init(StorageService storageService) {
        return (args) -> {
            storageService.deleteAll();
            storageService.init();
        };
    }*/
}